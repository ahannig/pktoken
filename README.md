# pktoken - create or verify a public key token                     

This tool can be used to create a random token, signed
with an ed25519 private key.

```                                                                    
USAGE:                                                               
   pktoken [global options] command [command options] [arguments...] 
                                                                     
COMMANDS:                                                            
   init     create a new public / private key pair                   
   create   create a new token with a private key                    
   verify   verify a token with a pubkey; verify <key> <token>       
   help, h  Shows a list of commands or help for one command         
                                                                     
```
