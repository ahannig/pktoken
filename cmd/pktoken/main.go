package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
)

func main() {

	app := &cli.App{
		Name:  "pktoken",
		Usage: "create or verify a public key token",
		Commands: []*cli.Command{
			{
				Name:   "init",
				Usage:  "create a new public / private key pair",
				Action: createKeyPair,
			},
			{
				Name:   "create",
				Usage:  "create a new token with a private key",
				Action: createToken,
			},
			{
				Name:   "verify",
				Usage:  "verify a token with a pubkey; verify <key> <token>",
				Action: verifyToken,
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
