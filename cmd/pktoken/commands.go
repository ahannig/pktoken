package main

import (
	"fmt"

	"github.com/urfave/cli/v2"

	"gitlab.com/ahannig/pktoken/pkg/token"
)

func createKeyPair(ctx *cli.Context) error {
	pub, priv, err := token.GenerateKey()
	if err != nil {
		return err
	}
	fmt.Println("PRV:", priv)
	fmt.Println("PUB:", pub)
	return nil
}

func createToken(ctx *cli.Context) error {
	if !ctx.Args().Present() {
		return fmt.Errorf("require private key")
	}
	key := ctx.Args().First()
	token, err := token.Create(key)
	fmt.Println(token)
	return err
}

func verifyToken(ctx *cli.Context) error {
	if ctx.Args().Len() < 2 {
		return fmt.Errorf("require public key and token")
	}
	pubkey := ctx.Args().Get(0)
	t := ctx.Args().Get(1)

	if err := token.Verify(pubkey, t); err != nil {
		return err
	}
	fmt.Println("OK")
	return nil
}
