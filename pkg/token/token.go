package token

import (
	"crypto/ed25519"
	"crypto/rand"
	"encoding/base64"
	"errors"
)

const (
	// TokenMsgLen is the length of the msg in bytes
	TokenMsgLen = 8
)

// ErrInvalidToken will be the result of a failed verify
var ErrInvalidToken = errors.New("token verification failed")

// GenerateKey creates a new public private key pair and
// returns it encoded as urlsafe base64
func GenerateKey() (string, string, error) {
	pub, priv, err := ed25519.GenerateKey(nil)
	if err != nil {
		return "", "", err
	}

	pubEnc := base64.URLEncoding.EncodeToString([]byte(pub))
	privEnc := base64.URLEncoding.EncodeToString([]byte(priv))

	return pubEnc, privEnc, nil
}

// Create creates a token and signs it
func Create(key string) (string, error) {
	privKey, err := base64.URLEncoding.DecodeString(key)
	if err != nil {
		return "", err
	}

	msg := make([]byte, TokenMsgLen)
	if _, err := rand.Read(msg); err != nil {
		panic(err)
	}
	sig := ed25519.Sign(ed25519.PrivateKey(privKey), msg)
	token := append(msg, sig...)
	return base64.URLEncoding.EncodeToString(token), nil
}

// Verify verifies a token with a public key
func Verify(key, token string) error {
	pubKey, err := base64.URLEncoding.DecodeString(key)
	if err != nil {
		return err
	}
	data, err := base64.URLEncoding.DecodeString(token)
	if err != nil {
		return err
	}
	msg := data[:TokenMsgLen]
	sig := data[TokenMsgLen:]

	if !ed25519.Verify(pubKey, msg, sig) {
		return ErrInvalidToken
	}
	return nil
}
