package token

import (
	"testing"
)

func TestCreateTokenVerify(t *testing.T) {
	pub, priv, err := GenerateKey()
	if err != nil {
		t.Fatal(err)
	}

	token, err := Create(priv)
	if err != nil {
		t.Error(err)
	}
	t.Log(token)

	if err := Verify(pub, token); err != nil {
		t.Fatal(err)
	}
}
